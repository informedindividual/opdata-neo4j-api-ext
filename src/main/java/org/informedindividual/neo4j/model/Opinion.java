package org.informedindividual.neo4j.model;

import com.fasterxml.jackson.annotation.JsonTypeName;
import org.neo4j.graphdb.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Opinion extends IINode {

    String title;
    String description;
    Float score;
    String category;

    public Opinion(Node node){
        this(node, new ArrayList<>());
    }

    public Opinion(Node node, List<Relationship> relationships){
        super(node, relationships);
        Map<String,Object> allProperties = node.getAllProperties();
        this.title = (String) allProperties.get("title");
        this.description = (String) allProperties.get("description");
        this.score = Float.parseFloat((String)allProperties.get("score"));
        this.category = (String) allProperties.get("category");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
