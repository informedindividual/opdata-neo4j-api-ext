package org.informedindividual.neo4j.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import org.neo4j.graphdb.Node;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value=Product.class, name="Product"),
        @JsonSubTypes.Type(value=Opinion.class, name="Opinion"),
        @JsonSubTypes.Type(value=Relationship.class, name="Relationship")
})
public abstract class IINode {

    public String iiid = "";
    public List<Relationship> relationships = new ArrayList<>();

    protected IINode(Node node){
        Map<String,Object> allProperties = node.getAllProperties();
        this.iiid = (String)allProperties.get("iiid");
    }

    protected IINode(String iiid){
        this.iiid = iiid;
    }

    protected IINode(Node node, List<Relationship> relationships){
        Map<String,Object> allProperties = node.getAllProperties();
        this.iiid = (String)allProperties.get("iiid");
        this.relationships = relationships;
    }

    public String getIiid() {
        return iiid;
    }

    public void setIiid(String iiid) {
        this.iiid = iiid;
    }

    public List<Relationship> getRelationships() {
        return relationships;
    }

    public void setRelationships(List<Relationship> relationships) {
        this.relationships = relationships;
    }

    public void writeRelationships(JsonGenerator gen) throws IOException{
        if (relationships.size()>0){
            gen.writeArrayFieldStart("relationships");
            Iterator<Relationship> relIt = relationships.iterator();
            while (relIt.hasNext()){
                Relationship relationship = relIt.next();
                gen.writeObject(relationship);
            }
            gen.writeEndArray();
        }
    }
}

