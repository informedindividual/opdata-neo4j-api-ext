package org.informedindividual.neo4j.model;

import org.neo4j.graphdb.Node;

import java.util.Map;

public class ProductCategory extends IINode {


    private String title;
    private String imageUrl;

    public ProductCategory(Node node) {
        super(node);
        Map<String,Object> allProperties = node.getAllProperties();
        this.title = (String)allProperties.get("title");
        this.imageUrl = (String)allProperties.get("imageUrl");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
