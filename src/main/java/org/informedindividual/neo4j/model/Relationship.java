package org.informedindividual.neo4j.model;

import org.neo4j.graphdb.Node;

import java.util.Map;

public class Relationship extends IINode {

    String relationshipType;
    String startnode;
    String endnode;
    String targetType;

    public Relationship(Map<String,Object> allProperties){
        super((String)allProperties.get("iiid"));
        this.relationshipType = (String) allProperties.get("relationshipType");
        this.startnode = (String) allProperties.get("startnode");
        this.endnode = (String) allProperties.get("endnode");
        this.targetType = (String) allProperties.get("targetType");

    }

    public String getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }

    public String getStartnode() {
        return startnode;
    }

    public void setStartnode(String startnode) {
        this.startnode = startnode;
    }

    public String getEndnode() {
        return endnode;
    }

    public void setEndnode(String endnode) {
        this.endnode = endnode;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }
}
