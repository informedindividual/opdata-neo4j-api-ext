package org.informedindividual.neo4j.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.neo4j.graphdb.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Product extends IINode {

    public Product(Node node){
        this(node, new ArrayList<>());
    }

    public Product(Node node, List<Relationship> relationships){
        super(node, relationships);
        Map<String,Object> allProperties = node.getAllProperties();
        this.gtin = (String)allProperties.get("gtin");
        this.title = (String)allProperties.get("title");
        this.subtitle = (String)allProperties.get("subtitle");
        this.ingredients = (String)allProperties.get("ingredients");
        this.imageUrl = (String)allProperties.get("imageUrl");
    }

    private String gtin;
    private String title;
    private String subtitle;
    private String ingredients;
    private String imageUrl;

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
