package org.informedindividual.neo4j;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.logging.Log;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//START SNIPPET: HelloWorldResource
@Path( "/api" )
public class Api
{

    final ApiService apiService;
    final Log log;

    private static final RelationshipType PRODUCED_BY = RelationshipType.withName( "PRODUCED_BY" );
    private static final Label Product = Label.label( "Product" );

    public Api(@Context GraphDatabaseService db, @Context Log log )
    {
        apiService = new ApiService(db, log);
        this.log = log;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
    @Path("content/get/{iiid}")
    public Response getByIiiid(@PathParam("iiid") String iiid)  throws Exception {
        return apiService.getByIiid(iiid,false);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
    @Path("content/get/{iiid}/relations")
    public Response getByIiidWithRelations(@PathParam("iiid") String iiid)  throws Exception {
        return apiService.getByIiid(iiid,true);
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON+";charset=UTF-8")
    @Path("products/{gtin}")
    public Response getProductByGtin(@PathParam("gtin") String gtin)  throws Exception {
        log.info("products/{gtin} " + gtin);
        return apiService.getProductByGtin(gtin);
    }

    /*@GET
    @Path("/product/{gtin}")
    public Response findProduct( @PathParam("gtin") final String gtin )
    {
        final Map<String, Object> params = MapUtil.map( "gtin", gtin );

        StreamingOutput stream = new StreamingOutput()
        {
            @Override
            public void write( OutputStream os ) throws IOException, WebApplicationException
            {
                JsonGenerator jg = mapper.getJsonFactory().createJsonGenerator( os, JsonEncoding.UTF8 );
                jg.writeStartObject();
                jg.writeFieldName( "hits" );
                jg.writeStartArray();

                try ( Transaction tx = db.beginTx();
                      Result result = db.execute( colleaguesQuery(), params ) )
                {
                    while ( result.hasNext() )
                    {
                        Map<String,Object> row = result.next();
                        jg.writeString( ((Node) row.getByIiid( "product" )).getProperty( "title" ).toString() );
                        jg.writeString( ((Node) row.getByIiid( "product" )).getProperty( "gtin" ).toString() );
                    }
                    tx.success();
                }

                jg.writeEndArray();
                jg.writeEndObject();
                jg.flush();
                jg.close();
            }
        };

        return Response.ok().entity( stream ).type( MediaType.APPLICATION_JSON ).build();
    }

    private String colleaguesQuery()
    {
        return "MATCH (product:Product {gtin: $gtin }) RETURN product";
    }*/

}