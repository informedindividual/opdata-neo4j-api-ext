package org.informedindividual.neo4j;


import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import org.informedindividual.neo4j.model.*;
import org.informedindividual.neo4j.serializers.CustomObjectMapper;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.helpers.collection.MapUtil;
import org.neo4j.logging.Log;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ApiService {

    private GraphDatabaseService db;
    private org.neo4j.logging.Log log;
    final CustomObjectMapper customMapper = new CustomObjectMapper();

    public ApiService(GraphDatabaseService db, Log log) {
        this.db = db;
        this.log = log;
    }

    private String productByGtinQuery() {
        return "MATCH (product:Product {gtin: $gtin }) RETURN product";
    }
    private String getNodeByIiidQuery(){return "MATCH (node {iiid: $iiid }) RETURN node, labels(node)[0] as type";}
    private String getNodeRelationsByIiidQuery(){return "MATCH (node {iiid: $iiid})-[r]-(rt) RETURN " +
            "collect ({iiid:r.iiid,relationshipType:type(r), startnode: startnode(r).iiid, endnode:endnode(r).iiid, " +
            "targetType:labels(rt)[0]}) as relations";}

    public Response getProductByGtin(String gtin) {
        final Map<String, Object> params = MapUtil.map("gtin", gtin);

        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                JsonGenerator jg = customMapper.getFactory().createGenerator(output, JsonEncoding.UTF8);
                log.info("stream execute " + productByGtinQuery());
                log.info("params " + params.toString());
                try (Transaction tx = db.beginTx();
                    Result result = db.execute(productByGtinQuery(), params)) {
                    while (result.hasNext()) {
                        Map<String, Object> row = result.next();
                        Node node = (Node)row.get("product");
                        Product product = new Product(node);
                        jg.writeObject(product);
                        tx.success();
                    }
                }
                jg.flush();
                jg.close();
            }
        };
       return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
    }

    public Response getByIiid(String iiid, boolean includeRelations) {
        final Map<String, Object> params = MapUtil.map("iiid", iiid);

        StreamingOutput stream = new StreamingOutput() {
            @Override
            public void write(OutputStream output) throws IOException, WebApplicationException {
                JsonGenerator jg = customMapper.getFactory().createGenerator(output, JsonEncoding.UTF8);

                try (Transaction tx = db.beginTx();
                     Result result = db.execute(getNodeByIiidQuery(), params)) {
                        List<Relationship> relationships = new ArrayList<>();
                        if (includeRelations){
                            try (Transaction txRel = db.beginTx();
                                 Result rel = db.execute(getNodeRelationsByIiidQuery(), params)) {
                                 while (rel.hasNext()){
                                     Map<String,Object> row = rel.next();
                                     log.info("relations: "+row.get("relations"));
                                     List<Map<String,Object>> relations = (List<Map<String,Object>> )row.get("relations");
                                     Iterator<Map<String,Object>> relIt = relations.iterator();
                                     while (relIt.hasNext()){
                                         Map<String,Object> relNode = relIt.next();
                                         relationships.add(new Relationship(relNode));
                                     }
                                 }
                                txRel.success();
                            }
                        }

                        IINode iiNode;
                        Map<String, Object> row = result.next();
                        Node node = (Node)row.get("node");
                        String type = (String)row.get("type");

                        if (type.equals("Product")){
                            iiNode = new Product(node, relationships);
                        }else if (type.equals("Opinion")){
                            iiNode = new Opinion(node, relationships);
                        }else {
                            iiNode = null;
                        }

                        jg.writeObject(iiNode);
                        tx.success();
                }
                jg.flush();
                jg.close();
            }
        };
        return Response.ok().entity(stream).type(MediaType.APPLICATION_JSON).build();
    }
}
