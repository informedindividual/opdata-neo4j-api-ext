package org.informedindividual.neo4j.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.informedindividual.neo4j.model.Product;
import org.neo4j.graphdb.Node;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

class NodeSerializer extends StdSerializer<Node> {

    protected NodeSerializer() {
        this(null);
    }

    protected NodeSerializer(Class<Node> t) {
        super(t);
    }

    @Override
    public void serialize(Node value, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {
        Map<String,Object> allProperties = value.getAllProperties();

        gen.writeStartObject();
        gen.writeStringField("iiid", (String)value.getProperty("iiid"));
        gen.writeStringField("type", value.getLabels().iterator().next().toString());
        gen.writeObjectFieldStart("data");
        Iterator<String> it = allProperties.keySet().iterator();
        while (it.hasNext()){
            String key = it.next();
            if (key.equals("iiid"))continue;
            String val = (String)allProperties.get(key);
            gen.writeStringField(key, val);
        }
        gen.writeEndObject();
        gen.writeEndObject();
    }
}

