package org.informedindividual.neo4j.serializers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.informedindividual.neo4j.model.Opinion;
import org.informedindividual.neo4j.model.Product;
import org.informedindividual.neo4j.model.Relationship;


public class CustomObjectMapper extends ObjectMapper{

    public CustomObjectMapper() {
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Product.class, new ProductSerializer());
        simpleModule.addSerializer(Opinion.class, new OpinionSerializer());
        simpleModule.addSerializer(Relationship.class, new RelationshipSerializer());
        registerModule(simpleModule);
        enable(SerializationFeature.INDENT_OUTPUT);
        enableDefaultTyping(DefaultTyping.NON_FINAL);
    }
}
