package org.informedindividual.neo4j.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import org.informedindividual.neo4j.model.IINode;
import org.informedindividual.neo4j.model.Product;
import org.informedindividual.neo4j.model.Relationship;

import javax.management.relation.Relation;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

class ProductSerializer extends JsonSerializer<Product> {

    protected ProductSerializer() {
        this(null);
    }

    protected ProductSerializer(Class<Product> t) {
        super();
    }

    @Override
    public void serialize(Product value, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {
        gen.writeStringField("iiid", value.getIiid());
        gen.writeObjectFieldStart("data");
        gen.writeStringField("gtin", value.getGtin());
        gen.writeStringField("title", value.getTitle());
        gen.writeStringField("subtitle", value.getSubtitle());
        gen.writeStringField("ingredients", value.getIngredients());
        gen.writeStringField("imageUrl", value.getImageUrl());
        gen.writeEndObject();
        value.writeRelationships(gen);
    }

    @Override
    public void serializeWithType(Product value, JsonGenerator gen,
                                  SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {

        typeSer.writeTypePrefixForObject(value, gen);
        serialize(value, gen, provider); // call your customized serialize method
        typeSer.writeTypeSuffixForObject(value, gen);
    }


}

