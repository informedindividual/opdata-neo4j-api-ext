package org.informedindividual.neo4j.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import org.informedindividual.neo4j.model.Opinion;
import org.informedindividual.neo4j.model.Relationship;

import java.io.IOException;
import java.util.List;

class RelationshipSerializer extends JsonSerializer<Relationship> {

    protected RelationshipSerializer() {
        this(null);
    }

    protected RelationshipSerializer(Class<Relationship> t) {
        super();
    }

    @Override
    public void serialize(Relationship value, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {
        gen.writeStringField("iiid", value.getIiid());
        gen.writeObjectFieldStart("data");
        gen.writeStringField("startnode", value.getStartnode());
        gen.writeStringField("endnode", value.getEndnode());
        gen.writeStringField("relationshipType", value.getRelationshipType());
        gen.writeStringField("targetType", value.getTargetType());
        gen.writeEndObject();
    }

    @Override
    public void serializeWithType(Relationship value, JsonGenerator gen,
                                  SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {

        typeSer.writeTypePrefixForObject(value, gen);
        serialize(value, gen, provider); // call your customized serialize method
        typeSer.writeTypeSuffixForObject(value, gen);
    }


}

