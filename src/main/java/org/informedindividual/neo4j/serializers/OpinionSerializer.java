package org.informedindividual.neo4j.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import org.informedindividual.neo4j.model.Opinion;
import org.informedindividual.neo4j.model.Product;
import org.informedindividual.neo4j.model.Relationship;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

class OpinionSerializer extends JsonSerializer<Opinion> {

    protected OpinionSerializer() {
        this(null);
    }

    protected OpinionSerializer(Class<Opinion> t) {
        super();
    }

    @Override
    public void serialize(Opinion value, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {
        gen.writeStringField("iiid", value.getIiid());
        gen.writeObjectFieldStart("data");
        gen.writeStringField("title", value.getTitle());
        gen.writeStringField("description", value.getDescription());
        gen.writeNumberField("score", value.getScore());
        gen.writeStringField("category", value.getCategory());
        gen.writeEndObject();
        value.writeRelationships(gen);
    }

    @Override
    public void serializeWithType(Opinion value, JsonGenerator gen,
                                  SerializerProvider provider, TypeSerializer typeSer)
            throws IOException, JsonProcessingException {

        typeSer.writeTypePrefixForObject(value, gen);
        serialize(value, gen, provider); // call your customized serialize method
        typeSer.writeTypeSuffixForObject(value, gen);
    }


}

