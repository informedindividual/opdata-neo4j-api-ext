#Neo4j API unmanaged extension

This project is a start to migrate existing API 
https://bitbucket.org/informedindividual/apirspctiv (private)
that is depending on Enonic XP to a unmanaged neo4j extension 
that will exist directly on top of Neo4j.

Ref:
https://neo4j.com/docs/java-reference/current/#server-unmanaged-extensions

#JacksonPolymorphicDeserialization
https://github.com/FasterXML/jackson-docs/wiki/JacksonPolymorphicDeserialization